/*eslint-disable*/
// chakra imports
import {
  Box, useColorModeValue, HStack
} from "@chakra-ui/react";
import React, {useState} from "react";
import SidebarContent from "./SidebarContent";
import MinSidebarContent from "./MinSidebarContent";
import {IoIosArrowDropleft, IoIosArrowDropright} from 'react-icons/io'
import { CreativeTimLogo } from "../Icons/Icons";
import { Separator } from "../Separator/Separator";
// FUNCTIONS

function Sidebar(props) {
  // to check for active links and opened collapses
  const mainPanel = React.useRef();
  let variantChange = "0.2s linear";
  const [miniSidebar, setMiniSidebar] = useState(true);

  const { logoText, routes, sidebarVariant } = props;

  //  BRAND
  //  Chakra Color Mode
  /* let sidebarBg = "none"; */
  let sidebarBg = useColorModeValue("white", "gray.700");
  let sidebarRadius = "0px";
  let sidebarMargins = "0px";
  if (sidebarVariant === "opaque") {
    sidebarBg = useColorModeValue("white", "gray.700");
    sidebarRadius = "16px";
    sidebarMargins = "16px 0px 16px 16px";
  }

  // SIDEBAR
  return (
    <Box ref={mainPanel} >
      <Box display={{ sm: "none", xl: "block" }} position="fixed"  zIndex={'999'} >
        <Box
          bg={sidebarBg}
          transition={variantChange}
          w={miniSidebar ? '100px' : '240px'}
          maxW="260px"
         /*  ms={!miniSidebar ?{
            sm: "16px",
          } : null} */
          /* my={{
            sm: "16px",
          }} */
          mt={'68px'}
          h="calc(100vh)"
          ps="20px"
          pe="20px"
        /*   m={sidebarMargins} */
          borderRadius={sidebarRadius}
          onMouseEnter={() => setMiniSidebar(false)}
          onMouseLeave={() => setMiniSidebar(true)}
         /*  border={'1px solid red'} */
        >
         
       
       {/* <Box pt={"25px"} mb="12px" transition='0.2s linear'> */}
      {/* <HStack
        

        display="flex"
        lineHeight="100%"
        mb="30px"
        fontWeight="bold"
        justifyContent="center"
        alignItems="center"
        fontSize="11px"
        position={'relative'}
        
      > */}
       {/*  <CreativeTimLogo w="32px" h="32px"  /> */}
      {/*  { miniSidebar ? <IoIosArrowDropright fontSize={'20px'} style={{position:'absolute', left: '62px', bottom:'5px', cursor:'pointer'}}
        onClick={() => setMiniSidebar(!miniSidebar)}
        /> : <IoIosArrowDropleft fontSize={'20px'} style={{ position:'absolute', left: '221px', bottom:'5px', cursor:'pointer'}}
        onClick={() => setMiniSidebar(!miniSidebar)}
        />} */}
       {/*  <Text fontSize="sm" mt="3px">
          {logoText}
        </Text> */}
     {/*  </HStack> */}
      {/* <Separator></Separator> */}
    {/* </Box> */}
    <SidebarContent routes={routes}
        logoText={"PURITY UI DASHBOARD"}
        display="none"
        sidebarVariant={sidebarVariant}
        miniSidebar={miniSidebar}
        />
      {/*  { miniSidebar ?  <MinSidebarContent routes={routes}
        logoText={null}
        display="none"
        sidebarVariant={sidebarVariant}
        setMiniSidebar = {setMiniSidebar}
        miniSidebar = {miniSidebar}
        /> :
  <SidebarContent routes={routes}
        logoText={"PURITY UI DASHBOARD"}
        display="none"
        sidebarVariant={sidebarVariant}
        /> } */}
       
       

        </Box>
      </Box>
    </Box>
  );
}




export default Sidebar;
