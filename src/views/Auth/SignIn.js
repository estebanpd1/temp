import React, { useState } from 'react';
// Chakra imports
import {
  Box,
  Flex,
  Button,
  FormControl,
  FormLabel,
  Heading,
  Input,
  Link,
  Text,
  useColorModeValue,
  useToast,
  VStack,
  FormErrorMessage,
  InputGroup,
  InputLeftElement,
  InputRightElement,
  IconButton,
  Fade,
} from '@chakra-ui/react';
import { useNavigate, Link as RouteLink } from 'react-router-dom';
import { Formik, Field } from 'formik';
import { BiUserCircle } from 'react-icons/bi';
import { RiLockPasswordLine } from 'react-icons/ri';
import { GrFormViewHide, GrFormView } from 'react-icons/gr';
import { RiEyeCloseLine, RiEyeLine } from 'react-icons/ri';
// Assets
import signInImage from '../../assets/img/EHR.png';

function SignIn() {
  // Chakra color mode
  const titleColor = useColorModeValue('#002c5c', 'gray.100');
  const textColor = useColorModeValue('gray.400', 'white');
  const [status, setStatus] = useState(false);
  const [showPass, setShowPass] = useState(false);

  const handleShowPass = () => setShowPass(!showPass);

  const toast = useToast();

  let navigateTo = useNavigate();

  return (
    <Flex position="relative" mb="40px">
      <Flex
        h={{
          sm: 'initial',
          md: '75vh',
          lg: '85vh',
        }}
        w="100%"
        maxW="1044px"
        mx="auto"
        justifyContent="space-between"
        mb="30px"
        pt={{
          sm: '100px',
          md: '0px',
        }}
      >
        <Flex
          alignItems="center"
          justifyContent="start"
          style={{ userSelect: 'none' }}
          w={{
            base: '100%',
            md: '50%',
            lg: '42%',
          }}
        >
          <Flex
            direction="column"
            w="100%"
            background="transparent"
            p="48px"
            mt={{
              md: '150px',
              lg: '80px',
            }}
          >
            <Heading color={titleColor} fontSize="32px" mb="10px">
              Bienvenidos
            </Heading>
            <Text
              mb="36px"
              ms="4px"
              color={textColor}
              fontWeight="bold"
              fontSize="14px"
            >
              Ingresa tu usuario y contraseña
            </Text>
            <Formik
              initialValues={{
                username: '',
                password: '',
              }}
              onSubmit={values => {
                setStatus(true);

                setTimeout(() => {
                  if (
                    values.username === 'Test1' &&
                    values.password === '1234'
                  ) {
                    navigateTo('/admin/dashboard');
                  } else {
                    setStatus(false);
                    toast({
                      description:
                        'El usuario y/o contraseña no son correctos.',
                      status: 'warning',
                      duration: 9000,
                      isClosable: true,
                      position: 'top',
                      variant: 'left-accent',
                    });
                  }
                }, 2000);
              }}
            >
              {({ handleSubmit, errors, touched }) => (
                <form onSubmit={handleSubmit}>
                  <FormControl
                    isInvalid={!!errors.username && touched.username}
                  >
                    <FormLabel ms="4px" fontSize="sm" fontWeight="normal">
                      Usuario
                    </FormLabel>
                    <InputGroup>
                      <InputLeftElement top={'4px'}>
                        <BiUserCircle fontSize={'24px'} color={'#acacac'} />
                      </InputLeftElement>
                      <Field
                        style={{ paddingLeft: '36px' }}
                        as={Input}
                        id="username"
                        type="text"
                        name="username"
                        placeholder="Test1"
                        validate={value => {
                          let error;
                          if (value.length < 1) {
                            error = 'Por favor ingresa un nombre de usuario';
                          }

                          return error;
                        }}
                        borderRadius="15px"
                        mb="24px"
                        fontSize="sm"
                        size="lg"
                      />
                    </InputGroup>
                    <Fade in={true}>
                      <FormErrorMessage mt={'-10px'} mb={5}>
                        {errors.username}
                      </FormErrorMessage>
                    </Fade>
                  </FormControl>

                  <FormControl
                    isInvalid={!!errors.password && touched.password}
                  >
                    <FormLabel ms="4px" fontSize="sm" fontWeight="normal">
                      Contraseña
                    </FormLabel>
                    <InputGroup>
                      <InputLeftElement top={'4px'}>
                        <RiLockPasswordLine
                          fontSize={'24px'}
                          color={'#acacac'}
                        />
                      </InputLeftElement>
                      <Field
                        style={{ paddingLeft: '36px' }}
                        as={Input}
                        id="password"
                        type={showPass ? 'text' : 'password'}
                        name="password"
                        placeholder="1234"
                        validate={value => {
                          let error;
                          if (value.length < 1) {
                            error = 'Por favor ingresa una contraseña';
                          }
                          return error;
                        }}
                        borderRadius="15px"
                        mb="24px"
                        fontSize="sm"
                        size="lg"
                      />
                      <InputRightElement top={'4px'}>
                        <IconButton
                          onClick={handleShowPass}
                          variant={'link'}
                          icon={
                            showPass ? (
                              <RiEyeLine color={'#acacac'} fontSize={'18px'} />
                            ) : (
                              <RiEyeCloseLine
                                color={'#acacac'}
                                fontSize={'18px'}
                              />
                            )
                          }
                        />
                      </InputRightElement>
                    </InputGroup>
                    <FormErrorMessage mt={'-10px'} mb={5}>
                      {errors.password}
                    </FormErrorMessage>
                  </FormControl>
                  {/*    <FormControl display='flex' alignItems='center'>
                                            <Switch id='remember-login' colorScheme='teal' me='10px'/>
                                            <FormLabel htmlFor='remember-login' mb='0' ms='1' fontWeight='normal'>
                                                Remember me
                                            </FormLabel>
                                        </FormControl>  */}
                  <Button
                    isLoading={status}
                    fontSize="14px"
                    type="submit"
                    bg="#002c5c"
                    w="100%"
                    h="45"
                    color="white"
                    my={6}
                    _hover={{ bg: '#5691d1' }}
                    _active={{ bg: '#505356' }}
                  >
                    INGRESAR
                  </Button>
                </form>
              )}
            </Formik>

            <Flex
              flexDirection="column"
              justifyContent="center"
              alignItems="center"
              maxW="100%"
              mt="0px"
            >
              <RouteLink to="/auth/recuperarContrasena">
                <Link color={titleColor} fontWeight="medium">
                  Recuperar contraseña{' '}
                  {/* <Link color={titleColor}
                                    as='span'
                                    ms='5px'
                                    fontWeight='bold'>
                                    Sign Up
                                </Link> */}{' '}
                </Link>
              </RouteLink>
            </Flex>
          </Flex>
        </Flex>
        <Box
          display={{
            base: 'none',
            md: 'block',
          }}
          overflowX="hidden"
          h="100%"
          w="40vw"
          position="absolute"
          right="0px"
        >
          <Box
            bgImage={signInImage}
            w="100%"
            h="100%"
            bgSize="cover"
            bgPosition="50%"
            position="absolute"
            borderBottomLeftRadius="20px"
          ></Box>
        </Box>
      </Flex>
    </Flex>
  );
}

export default SignIn;
