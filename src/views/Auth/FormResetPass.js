import React, {useState} from "react";
// Chakra imports
import {
    Box,
    Flex,
    Button,
    FormControl,
    FormLabel,
    Heading,
    Input,
    Link,

    Text,
    useColorModeValue,
    useToast,
    VStack,
    FormErrorMessage,
    InputGroup,
    InputLeftElement,
    InputRightElement,
    IconButton
} from "@chakra-ui/react";
import {useNavigate, Link as RouteLink} from "react-router-dom";
import {Formik, Field} from 'formik'
import {BiUserCircle} from 'react-icons/bi'
import {RiLockPasswordLine, RiMailLine} from 'react-icons/ri'
import {GrFormViewHide, GrFormView} from 'react-icons/gr'
import {MdEmail} from 'react-icons/md'
// Assets

import signInImage from '../../assets/img/EHR.png';

function FormResetPass() { // Chakra color mode
    const titleColor = useColorModeValue('#002c5c', 'gray.100');
    const textColor = useColorModeValue("gray.400", "white");
    const [status, setStatus] = useState(false);
    const [showPass, setShowPass] = useState(false);

    const handleShowPass = () => setShowPass(!showPass);

    const toast = useToast()

    let navigateTo = useNavigate();


    return (
        <Flex position='relative' mb='40px'>
            <Flex h={
                    {
                        sm: "initial",
                        md: "75vh",
                        lg: "85vh"
                    }
                }
                w='100%'
                maxW='1044px'
                mx='auto'
                justifyContent='space-between'
                mb='30px'
                pt={
                    {
                        sm: "100px",
                        md: "0px"
                    }
            }>
                <Flex alignItems='center' justifyContent='start'
                    style={
                        {userSelect: "none"}
                    }
                    w={
                        {
                            base: "100%",
                            md: "50%",
                            lg: "42%"
                        }
                }>
                    <Flex direction='column' w='100%' background='transparent' p='48px'
                        mt={
                            {
                                md: "150px",
                                lg: "80px"
                            }
                    }>
                        <Heading color={titleColor}
                            fontSize='32px'
                            mb='10px'>
                            Recuperar Contraseña
                        </Heading>
                        <Text mb='36px' ms='4px'
                            color={textColor}
                            fontWeight='bold'
                            fontSize='14px'>
                            Ingresa tu correo electronico
                        </Text>
                        <Formik initialValues={
                                {
                                    email: ''
                                    
                                }
                            }
                            onSubmit={
                                (values, { resetForm }) => {

                                    setStatus(true)

                                    setTimeout(() => {

                                        if (true) {

                                            setStatus(false)
                                            toast({
                                                title: `Correo enviado a ${values.email}`,
                                                description: "Por favor revise su correo electronico.",
                                                status: 'success',
                                                duration: 9000,
                                                isClosable: true,
                                                position: 'top',
                                                variant: 'left-accent'
                                            })

                                            resetForm();

                                            setTimeout(() => {
                                                navigateTo("/auth/resetPassword");
                                            }, 500);


                                        } else {
                                            setStatus(false)
                                            toast({
                                                title: 'Error',
                                                description: "El usuario y/o contraseña no son correctos.",
                                                status: 'error',
                                                duration: 9000,
                                                isClosable: true,
                                                position: 'top'
                                            })
                                        }

                                    }, 2000);


                                }
                        }>

                            {
                            ({handleSubmit, errors, touched}) => (
                                <form onSubmit={handleSubmit}>


                                    <FormControl isInvalid={
                                        !!errors.email && touched.email
                                    }>


                                        <FormLabel ms='4px' fontSize='sm' fontWeight='normal'>
                                            Correo electronico
                                        </FormLabel>
                                        <InputGroup>
                                            <InputLeftElement top={'4px'}>
                                                <MdEmail fontSize={'24px'}
                                                    color={'#acacac'}/>
                                            </InputLeftElement>
                                            <Field style={
                                                    {paddingLeft: '36px'}
                                                }
                                                as={Input}
                                                id='email'
                                                type='text'
                                                name='email'
                                                placeholder='Test1@mail.com'

                                                validate={
                                                    (value) => {
                                                        let error;
                                                        if (value.length < 1) {
                                                            error = 'Por favor ingresa un correo electronico'
                                                        }else if (!/^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+\.[a-zA-Z0-9-.]+$/.test(value)) {
                                                            error = 'Por favor ingrese un correo valido.'
                                                        }

                                                        return error;
                                                    }
                                                }
                                                borderRadius='15px'
                                                mb='24px'
                                                fontSize='sm'
                                                size='lg'/>


                                        </InputGroup>
                                    <FormErrorMessage mt={'-10px'}
                                        mb={5}>
                                        {
                                        errors.email
                                    }</FormErrorMessage>
                                </FormControl>

                          
                            <Button isLoading={status}
                                fontSize='14px'
                                type='submit'
                                bg='#002c5c'
                                w='100%'
                                h='45'
                                color='white'
                                my={6}
                                _hover={
                                    {bg: "#5691d1"}
                                }
                                _active={
                                    {bg: "#505356"}
                            }>
                                ENVIAR
                            </Button>


                        </form>
                            )
                        } 
                        </Formik>


                       
                    </Flex>
                </Flex>
                <Box display={
                        {
                            base: "none",
                            md: "block"
                        }
                    }
                    overflowX='hidden'
                    h='100%'
                    w='40vw'
                    position='absolute'
                    right='0px'>
                    <Box bgImage={signInImage}
                        w='100%'
                        h='100%'
                        bgSize='cover'
                        bgPosition='50%'
                        position='absolute'
                        borderBottomLeftRadius='20px'></Box>
                </Box>
            </Flex>
        </Flex>
    );
}

export default FormResetPass;
