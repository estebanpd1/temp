import React, {useState} from "react";
// Chakra imports
import {
    Box,
    Flex,
    Button,
    FormControl,
    FormLabel,
    Heading,
    Input,
    Link,

    Text,
    useColorModeValue,
    useToast,
    VStack,
    FormErrorMessage,
    InputGroup,
    InputLeftElement,
    InputRightElement,
    IconButton
} from "@chakra-ui/react";
import {useNavigate, Link as RouteLink} from "react-router-dom";
import {Formik, Field} from 'formik'
import {BiUserCircle} from 'react-icons/bi'
import {RiLockPasswordLine} from 'react-icons/ri'
import {GrFormViewHide, GrFormView} from 'react-icons/gr'
import {RiEyeCloseLine, RiEyeLine} from 'react-icons/ri'
// Assets

import signInImage from '../../assets/img/EHR.png';

function ResetPassword() { // Chakra color mode
    const titleColor = useColorModeValue('#002c5c', 'gray.100');
    const textColor = useColorModeValue("gray.400", "white");
    const [status, setStatus] = useState(false);
    const [showPass, setShowPass] = useState(false);
    const [showPass2, setShowPass2] = useState(false);

    const handleShowPass = () => setShowPass(!showPass);
    const handleShowPass2 = () => setShowPass2(!showPass2);

    const toast = useToast()

    let navigateTo = useNavigate();


    return (
        <Flex position='relative' mb='40px'>
            <Flex h={
                    {
                        sm: "initial",
                        md: "75vh",
                        lg: "85vh"
                    }
                }
                w='100%'
                maxW='1044px'
                mx='auto'
                justifyContent='space-between'
                mb='30px'
                pt={
                    {
                        sm: "100px",
                        md: "0px"
                    }
            }>
                <Flex alignItems='center' justifyContent='start'
                    style={
                        {userSelect: "none"}
                    }
                    w={
                        {
                            base: "100%",
                            md: "50%",
                            lg: "42%"
                        }
                }>
                    <Flex direction='column' w='100%' background='transparent' p='48px'
                        mt={
                            {
                                md: "150px",
                                lg: "80px"
                            }
                    }>
                        <Heading color={titleColor}
                            fontSize='32px'
                            mb='10px'>
                            Cambiar contraseña
                        </Heading>
                        <Text mb='36px' ms='4px'
                            color={textColor}
                            fontWeight='bold'
                            fontSize='14px'>
                            Ingresa una nueva contraseña
                        </Text>
                        <Formik initialValues={
                                {
                                    password1: '',
                                    password2: ''
                                }
                            }
                            onSubmit={
                                (values) => {

                                    setStatus(true)

                                    setTimeout(() => {

                                        if (values.password1 === values.password2) {

                                            toast({
                                                title: 'Constraseña cambiada con exito',
                                                description: "",
                                                status: 'success',
                                                duration: 9000,
                                                isClosable: true,
                                                position: 'top',
                                                variant: 'left-accent'
                                            })

                                           
                                            setTimeout(() => {
                                                navigateTo("/admin/dashboard")
                                            }, 600);


                                        } else {
                                            setStatus(false)
                                            toast({
                                                
                                                description: "Las contraseñas no coinciden",
                                                status: 'error',
                                                duration: 9000,
                                                isClosable: true,
                                                position: 'top',
                                                variant: 'left-accent'
                                            })
                                        }

                                    }, 2000);


                                }
                        }>

                            {
                            ({handleSubmit, errors, touched}) => (
                                <form onSubmit={handleSubmit}>


                                    <FormControl isInvalid={
                                        !!errors.password1 && touched.password1
                                    }>


                                        <FormLabel ms='4px' fontSize='sm' fontWeight='normal'>
                                            Contraseña
                                        </FormLabel>
                                        <InputGroup>
                                            <InputLeftElement top={'4px'}>
                                                <RiLockPasswordLine fontSize={'24px'}
                                                    color={'#acacac'}/>
                                            </InputLeftElement>
                                            <Field style={
                                                    {paddingLeft: '36px'}
                                                }
                                                as={Input}
                                                id='password1'
                                                type={ showPass ? 'text' : 'password' }
                                                name='password1'
                                                placeholder='1234'

                                                validate={
                                                    (value) => {
                                                        let error;
                                                        if (value.length < 1) {
                                                            error = 'Por favor ingresa una contraseña'
                                                        }

                                                        return error;
                                                    }
                                                }
                                                borderRadius='15px'
                                                mb='24px'
                                                fontSize='sm'
                                                size='lg'/>

                                                 <InputRightElement top={'4px'}>
                                            <IconButton 
                                            onClick={handleShowPass}
                                            variant={'link'}
                                            icon={showPass ?   <RiEyeLine  color={"#acacac"} fontSize={'18px'} /> : <RiEyeCloseLine color={"#acacac"} fontSize={'18px'}/>  }
                                           
                                            />
                                            </InputRightElement>


                                        </InputGroup>
                                    <FormErrorMessage mt={'-10px'}
                                        mb={5}>
                                        {
                                        errors.password1
                                    }</FormErrorMessage>
                                </FormControl>

                                <FormControl isInvalid={
                                    !!errors.password2 && touched.password2
                                }>
                                    <FormLabel ms='4px' fontSize='sm' fontWeight='normal'>
                                        Repetir contraseña
                                    </FormLabel>
                                    <InputGroup>
                                        <InputLeftElement top={'4px'}>
                                            <RiLockPasswordLine fontSize={'24px'}
                                                color={'#acacac'}/>
                                        </InputLeftElement>
                                        <Field style={
                                                {paddingLeft: '36px'}
                                            }
                                            as={Input}
                                            id='password2'
                                            type={ showPass2 ? 'text' : 'password' }
                                            name='password2'
                                            placeholder='1234'
                                            validate={
                                                (value) => {
                                                    let error;
                                                    if (value.length < 1) {
                                                        error = 'Por favor repetir contraseña'
                                                    }
                                                    return error;
                                                }
                                            }
                                            borderRadius='15px'
                                            mb='24px'
                                            fontSize='sm'
                                            size='lg'/>
                                            <InputRightElement top={'4px'}>
                                            <IconButton 
                                            onClick={handleShowPass2}
                                            variant={'link'}
                                            icon={showPass2 ?   <RiEyeLine  color={"#acacac"} fontSize={'18px'} /> : <RiEyeCloseLine color={"#acacac"} fontSize={'18px'}/>  }
                                           
                                            />
                                            </InputRightElement>

                                    </InputGroup>
                                <FormErrorMessage mt={'-10px'}
                                    mb={5}>
                                    {
                                    errors.password2
                                }</FormErrorMessage>
                            </FormControl>
                            {/*    <FormControl display='flex' alignItems='center'>
                                            <Switch id='remember-login' colorScheme='teal' me='10px'/>
                                            <FormLabel htmlFor='remember-login' mb='0' ms='1' fontWeight='normal'>
                                                Remember me
                                            </FormLabel>
                                        </FormControl>  */}
                            <Button isLoading={status}
                                fontSize='14px'
                                type='submit'
                                bg="#002c5c"
                                w='100%'
                                h='45'
                                color='white'
                                my={6}
                                _hover={
                                    {bg: "#5691d1"}
                                }
                                _active={
                                    {bg: "#505356"}
                            }>
                                Enviar
                            </Button>


                        </form>
                            )
                        } 
                        </Formik>


                        {/* <Flex flexDirection='column' justifyContent='center' alignItems='center' maxW='100%' mt='0px'>

                            <RouteLink to='/auth/resetPassword'>
                            <Link color={titleColor}
                                fontWeight='medium'> */}
                               {/*  Recuperar contraseña */} {/* <Link color={titleColor}
                                    as='span'
                                    ms='5px'
                                    fontWeight='bold'>
                                    Sign Up
                                </Link> */}{/*  </Link>

                            </RouteLink>
                           
                        </Flex> */}
                    </Flex>
                </Flex>
                <Box display={
                        {
                            base: "none",
                            md: "block"
                        }
                    }
                    overflowX='hidden'
                    h='100%'
                    w='40vw'
                    position='absolute'
                    right='0px'>
                    <Box bgImage={signInImage}
                        w='100%'
                        h='100%'
                        bgSize='cover'
                        bgPosition='50%'
                        position='absolute'
                        borderBottomLeftRadius='20px'></Box>
                </Box>
            </Flex>
        </Flex>
    );
}

export default ResetPassword;
