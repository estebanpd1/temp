import React from 'react';
import {
  Table,
  Thead,
  Tbody,
  Tfoot,
  Tr,
  Th,
  Td,
  TableCaption,
  TableContainer,
  Button
} from '@chakra-ui/react';

export default function DataTable2({ columns, data }) {
  return (
  
      <Table variant='striped' size='sm'>
        <TableCaption>Imperial to metric conversion factors</TableCaption>
        <Thead >
          <Tr>
            {columns.map(col => (
              <Th>{col.Header}</Th>
            ))}
          </Tr>
        </Thead>
        <Tbody>
          {data.map(row => (
            <Tr>
              <Td>{row.id}</Td>
              <Td>{row.cliente}</Td>
              <Td>{row.ingreso}</Td>
              <Td>{row.egreso}</Td>
              <Td>{row.saldo}</Td>
              <Td>{row.imputacion}</Td>
              <Td>{row.detalle_mov}</Td>
              <Td>{row.ticket}</Td>
              <Td>{row.user}</Td>
              <Td>{row.estado_cajero ? 'Entregado' : 'Pendiente'}</Td>
              <Td>{row.estado_al_cliente ? 'Entregado' : 'Pendiente'}</Td>
              <Td>{row.fecha_operacion}</Td>
              <Td><Button onClick={()=>alert(`${row.id}`)}>Edit</Button></Td>
            </Tr>
          ))}
          
        </Tbody>
      
      </Table>
 
  );
}
