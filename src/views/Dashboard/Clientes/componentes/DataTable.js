import React, {useState, useMemo} from 'react'
import { 
    Flex,
    IconButton,
    Text,
    Tooltip,
    Select,
    NumberInput,
    NumberInputField,
  NumberInputStepper,
  NumberIncrementStepper,
  NumberDecrementStepper,
  useMediaQuery,
    Table, Thead, Tbody, Tr, Th, Td, chakra, Button , TableContainer, Box} from "@chakra-ui/react";
import { useTable, usePagination  } from 'react-table';
import {
    ArrowRightIcon,
    ArrowLeftIcon,
    ChevronRightIcon,
    ChevronLeftIcon
  } from "@chakra-ui/icons";


export default function DataTable({data, columns}) {

    const [isLargerThan600] = useMediaQuery('(min-width: 600px)')
    //const initialState = { hiddenColumns: ['detalle_mov'] };
    
    
    const col = useMemo(() => columns, []);
    const datos = useMemo(() => data, []);

    const tableInstance = useTable({
        columns: col,
        data : datos,
        initialState : {pageIndex: 2, hiddenColumns: ['detalle_mov']}
        
    },
    usePagination
    );

    

    const {
        getTableProps,
        getTableBodyProps,
        headerGroups,
      page,
        prepareRow,
        canPreviousPage,
        canNextPage,
        pageOptions,
        pageCount,
        gotoPage,
        nextPage,
        previousPage,
        setPageSize,
        state : {pageIndex, pageSize }
    
    } = tableInstance



  return (
    <>
    
   {/*  <TableContainer overflowX={'scroll'} h={'80vh'} overflowY={'auto'}> */}
    { isLargerThan600 ? <Table {...getTableProps()}  variant='striped' colorScheme={'gray'} size={'sm'} maxWidth={'100%'} boxShadow={'0 1px 3px rgba(0,0,0,0.2)'}>
        <Thead >
            {headerGroups.map((headerGroup)=>(
                  <Tr {...headerGroup.getHeaderGroupProps()} 
                 >
                 {
                    headerGroup.headers.map((column) => (
                        <Th {...column.getHeaderProps()}  fontWeight={'600'} color={'#ffffff'} bg={'#002C5C'}>
                      {column.render('Header')}
                     </Th>
                    ))
                    
                 }
                 <Th fontWeight={'600'} color={'#ffffff'} bg={'#002C5C'} >Opciones</Th> 
             
          </Tr>

            ))}
          
              
           
        </Thead>
        <Tbody {...getTableBodyProps()} >
           { page.map((row)=>{
            prepareRow(row)
            return(
                <Tr {...row.getRowProps()} >
                   {row.cells.map((cell)=> {
                    return (
                        <Td {...cell.getCellProps()} 
                       >
                            {(cell.column.Header === 'Estado al Cajero' || cell.column.Header === 'Estado al Cliente' )? (
                                cell.value ? 'Enviado' : 'Pendiente'
                            ):(
                                cell.render('Cell')
                            ) 
                            }
                        </Td>
                    )
                   })}
                   <Td  >
                    <Button>
                        Editar
                    </Button>
                   </Td>
               
            
        </Tr>
            )
           })}
              
           
        </Tbody>
    </Table> :
    <Table {...getTableProps()} display={'block'} variant={'unstyled'} size={'sm'} maxWidth={'100%'} boxShadow={'0 1px 3px rgba(0,0,0,0.2)'}>
    <Thead display={'block'}>
        {headerGroups.map((headerGroup)=>(
              <Tr {...headerGroup.getHeaderGroupProps()} display={'none'} >
             {
                headerGroup.headers.map((column) => (
                    <Th {...column.getHeaderProps()}  fontWeight={'600'} color={'#ffffff'} bg={'#002C5C'}display={'block'}>
                  {column.render('Header')}
                 </Th>
                ))
                
             }
             <Th fontWeight={'600'} color={'#ffffff'} bg={'#002C5C'} display={'block'}>Opciones</Th> 
         
      </Tr>

        ))}
      
          
       
    </Thead>
    <Tbody {...getTableBodyProps()} display='block'>
       { page.map((row)=>{
        prepareRow(row)
        return(
            <Tr {...row.getRowProps()} display={'block'} 
            _odd={{bg:'#eee'}}>
               {row.cells.map((cell)=> {
                console.log(cell.column.Header)
                return (
                    <Td {...cell.getCellProps()} display='block' 
                    borderBottom={'1px solid #eee'}
                    position={'relative'}
                    pl={'50%'}
                    data-title={cell.column.Header}
                    _before={{
                        
                            position: 'absolute',
                            content: `"${cell.column.Header}"`,
                            top: '2',
                            left: '6px',
                            width: '45%',
                           paddingRight: '10px',
                           
                        
                    }}>
                        {(cell.column.Header === 'Estado al Cajero' || cell.column.Header === 'Estado al Cliente' )? (
                            cell.value ? 'Enviado' : 'Pendiente'
                        ):(
                            cell.render('Cell')
                        ) 
                        
                        }
                    </Td>
                )
               })}
               <Td  display={'block'} 
                    borderBottom={'1px solid #eee'} 
                    position={'relative'}
                    pl={'50%'}
                    _before={{
                        
                        position: 'absolute',
                       
                        top: '0',
                        left: '6px',
                        width: '45%',
                       paddingRight: '10px',
                       
                    
                }}
                    >
                <Button>
                    Editar
                </Button>
               </Td>
           
        
    </Tr>
        )
       })}
          
       
    </Tbody>
</Table>  }

    <Flex justifyContent="space-between" m={4} alignItems="center">
        <Flex>
          
            <IconButton
              onClick={() => gotoPage(0)}
              isDisabled={!canPreviousPage}
              icon={<ArrowLeftIcon h={3} w={3} />}
              mr={4}
            />
         
          
            <IconButton
              onClick={previousPage}
              isDisabled={!canPreviousPage}
              icon={<ChevronLeftIcon h={6} w={6} />}
            />
          
        </Flex>

        <Flex alignItems="center">
          <Text flexShrink="0" mr={8}>
            Page{" "}
            <Text fontWeight="bold" as="span">
              {pageIndex + 1}
            </Text>{" "}
            of{" "}
            <Text fontWeight="bold" as="span">
              {pageOptions.length}
            </Text>
          </Text>
          <Text flexShrink="0">Go to page:</Text>{" "}
          <NumberInput
            ml={2}
            mr={8}
            w={28}
            min={1}
            max={pageOptions.length}
            onChange={(value) => {
              const page = value ? value - 1 : 0;
              gotoPage(page);
            }}
            defaultValue={pageIndex + 1}
          >
            <NumberInputField />
            <NumberInputStepper>
              <NumberIncrementStepper />
              <NumberDecrementStepper />
            </NumberInputStepper>
          </NumberInput>
          <Select
            w={32}
            value={pageSize}
            onChange={(e) => {
              setPageSize(Number(e.target.value));
            }}
          >
            {[10, 20, 30, 40, 50].map((pageSize) => (
              <option key={pageSize} value={pageSize}>
                Show {pageSize}
              </option>
            ))}
          </Select>
        </Flex>

        <Flex>
          
            <IconButton
              onClick={nextPage}
              isDisabled={!canNextPage}
              icon={<ChevronRightIcon h={6} w={6} />}
            />
        
          
            <IconButton
              onClick={() => gotoPage(pageCount - 1)}
              isDisabled={!canNextPage}
              icon={<ArrowRightIcon h={3} w={3} />}
              ml={4}
            />
        
        </Flex>
      </Flex>

   {/*  </TableContainer> */}
    
  
    </>
  )
}
