import { Box, Heading } from '@chakra-ui/react'
import React from 'react'

import DataTable from './componentes/DataTable'
import DataTable2 from './componentes/DataTable2'
import data from '../../../MOCK_DATA.json'



const columns = [
    {
        Header: 'Id',
        accessor: 'id'
    },
    {
        Header: 'Cliente',
        accessor: 'cliente'
    },
    {
        Header: 'Ingreso',
        accessor: 'ingreso'
    },
    {
        Header: 'Egreso',
        accessor: 'egreso'
    },
    {
        Header: 'Saldo',
        accessor: 'saldo'
    },
    {
        Header: 'Imputacion',
        accessor: 'imputacion'
    },
    {
        Header: 'Detalle Movimiento',
        accessor: 'detalle_mov'
    },
    {
        Header: 'N° Recibo',
        accessor: 'ticket'
    },
    {
        Header: 'Responsable',
        accessor: 'user'
    },
    {
        Header: 'Estado al Cajero',
        accessor: 'estado_cajero'
    },
    {
        Header: 'Estado al Cliente',
        accessor: 'estado_al_cliente'
    },
    {
        Header: 'Fecha',
        accessor: 'fecha_operacion'
    }
   ]

export default function Clientes() {

  return (
    <Box mt={'40px'}>
<Heading my={6}>Clientes</Heading>
<DataTable columns = {columns} data={data}/>
    </Box>
    
  )
}
