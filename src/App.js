import React from 'react'
import {Routes, Route, Navigate} from 'react-router-dom';
import SignIn from './views/Auth/SignIn';
import SignUp from './views/Auth/SignUp'
import FormResetPass from './views/Auth/FormResetPass';
import RestetPassword from './views/Auth/ResetPassword'
import Auth from './layouts/Auth';
import Admin from './layouts/Admin'
import Dashboard from './views/Dashboard/Dashboard';
import Clientes from './views/Dashboard/Clientes';

export default function App() {
  return (
    <Routes>
      <Route
        path="*"
        element={<Navigate to="/auth/Signin" replace />}
    />
      <Route path='/auth/*' element={<Auth />}>
        <Route path='Signin' element={<SignIn />} />
        <Route path='SignUp' element={<SignUp />} />
        <Route path='resetPassword' element={<RestetPassword />} />
        <Route path='recuperarContrasena' element={<FormResetPass />} />
      </Route>
      <Route path='/admin/*' element={<Admin />}>
        <Route path='dashboard' element={<Dashboard />} />
        <Route path='clientes' element={<Clientes />} />

      </Route>
    </Routes>
  )
}
